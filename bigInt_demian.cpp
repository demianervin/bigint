// Demian Ervin-Istvan
// June, 2020

#include <iostream>

using namespace std;

class BigInt
{
    int* t;
    int length, sign;
public:
    BigInt();                                       // alapertelmezett konstruktor
    BigInt(int sign, int n, const int* digits);     // adattagokat inicializalo konstruktor
    BigInt(const BigInt& v);                        // masolo konstruktor
    BigInt(int number);                             // implicit atalakitas egeszrol
    BigInt(long number);                            // implicit atalakitas lengthu egeszrol
    ~BigInt();                                      // destruktor
    class DivisionByZero {};                        // kivetel
    BigInt operator +(const BigInt& v);
    BigInt operator -(const BigInt& v);
    BigInt operator *(const BigInt& v);
    BigInt operator /(const BigInt& v);
    BigInt& operator ++();
    BigInt operator ++(int);
    BigInt& operator --();
    BigInt operator --(int);
    BigInt& operator =(const BigInt& v);
    BigInt& operator +=(const BigInt& v);
    BigInt& operator -=(const BigInt& v);
    BigInt& operator *=(const BigInt& v);
    BigInt& operator /=(const BigInt& v);
    ostream& kiir(ostream& s);

    bool operator >(const BigInt& v) const;
    bool operator ==(const BigInt& v) const;
};

BigInt::BigInt()
{
    length = 1;
    sign = 0;
    t = new int[1];
    t[0] = 0;
}

BigInt::BigInt(int sign, int n, const int* digits)
{
    this->sign = sign;
    length = n;
    t = new int[n];
    for (int i = 0; i < n; i++)
        t[n - i - 1] = digits[i];   // a digitset forditott sorrendben taroljuk
}

BigInt::BigInt(const BigInt& v)
{
    length = v.length;
    sign = v.sign;
    t = new int[length];
    for (int i = 0; i < length; i++)
        t[i] = v.t[i];
}

BigInt::BigInt(int number)
{
    if (number == 0)
    {
        length = 1;
        sign = 0;
        t = new int[1];
        t[0] = 0;
        return;
    }

    t = new int[20];
    length = 0;
    if (number < 0)
    {
        sign = -1;
        number = -number;
    }
    else sign = 1;

    while (number > 0)
    {
        int m = number % 10;
        number /= 10;
        length++;
        t[length - 1] = m;
    }
}

BigInt::BigInt(long number)
{
    if (number == 0)
    {
        length = 1;
        sign = 0;
        t = new int[1];
        t[0] = 0;
        return;
    }

    t = new int[20];
    length = 0;
    if (number < 0)
    {
        sign = -1;
        number = -number;
    }
    else sign = 1;

    while (number > 0)
    {
        int m = number % 10;
        number /= 10;
        length++;
        t[length - 1] = m;
    }
}

BigInt::~BigInt()
{
    delete[] t;
}

bool BigInt::operator>(const BigInt& v) const
{
    if (sign > v.sign) return 1;
    if (sign < v.sign) return 0;
    if (sign == -1)   // mindketto negativ
    {
        if (length > v.length) return 0;
        if (length < v.length) return 1;
        // ha negativak, egyforma lengthal
        int i = length - 1;
        while (i > 0 && t[i] == v.t[i]) i--;
        if (t[i] < v.t[i]) return 1;
        else return 0;
    }
    else if (sign == 1)
    {
        if (length > v.length) return 1;
        if (length < v.length) return 0;
        // ha pozitivak, egyforma lengthal
        int i = length - 1;
        while (i > 0 && t[i] == v.t[i]) i--;
        if (t[i] > v.t[i]) return 1;
        else return 0;
    }
    return 0;
}

bool BigInt::operator==(const BigInt& v) const
{
    if (sign != v.sign) return 0;
    if (length != v.length) return 0;
    for (int i = 0; i < length; i++) if (t[i] != v.t[i]) return 0;
    return 1;
}

BigInt& BigInt::operator=(const BigInt& v)
{
    sign = v.sign;
    length = v.length;
    t = new int[length];
    for (int i = 0; i < length; i++)
        t[i] = v.t[i];
    return *this;
}

BigInt BigInt::operator-(const BigInt& v)
{
    if (*this == v)
    {
        BigInt null;
        return null;
    }
    if (sign == 1 && v.sign == 1)
    {
        BigInt above(*this), under(v);
        int kiemel = 1, maxlength;
        // azt szeretnenk, hogy a nagyobb number keruljon "felulre"

        if (!(*this > v)) // ha nem jo a *this - v sorrend, kiemelunk -1-et
        {
            swap(above, under);
            kiemel = -1;
        }
        maxlength = above.length;

        int newlength = 0;
        int* newInt = new int[maxlength];

        int i = 0;
        while (i < under.length)
        {
            if (above.t[i] == -1)
            {
                above.t[i] = 9;
                above.t[i + 1]--;
            }
            if (above.t[i] < under.t[i])
            {
                above.t[i] += 10;
                above.t[i + 1]--;
            }
            newInt[newlength] = above.t[i] - under.t[i];
            newlength++;
            i++;
        }

        while (i < maxlength)
        {
            if (above.t[i] == -1)
            {
                above.t[i] = 9;
                above.t[i + 1]--;
            }
            newInt[newlength] = above.t[i];
            newlength++;
            i++;
        }

        while (newInt[newlength - 1] == 0) newlength--;     // kiszurjuk, hogy elso numberjegybol kolcsonkeres
                                                            // eseten 0-val kezdodo numberot kapjunk
        BigInt temp;
        temp.sign = kiemel;
        temp.length = newlength;
        temp.t = new int[newlength];
        for (int i = 0; i < newlength; i++)
            temp.t[i] = newInt[i];

        delete[] newInt;
        return temp;
    }
    else if (sign == 1 && v.sign == -1)
    {
        BigInt abs = v;
        abs.sign = 1;
        return *this + abs;
    }
    else if (sign == -1 && v.sign == 1) // pl. a=-1, b=3 --> a-b = -1-3 = -(1+3) = -(|a|+b);
    {
        BigInt absA = *this;
        absA.sign = 1;

        BigInt mApB = absA + v;

        BigInt result = mApB;
        result.sign = -1;

        return result;
    }
    else if (sign == -1 && v.sign == -1) // pl. a=-2, b=-3 --> a-b = -2+3 = 3-2 = |b| - |a|
    {
        BigInt absA = *this;
        absA.sign = 1;

        BigInt absB = v;
        absB.sign = 1;

        return absB - absA;
    }
    else    // ha valamelyik number = 0
    {
        if (sign == 0)
        {
            BigInt temp = v;
            temp.sign = -v.sign;
            return temp;
        }
        if (v.sign == 0)
            return *this;
    }
}

BigInt BigInt::operator+(const BigInt& v)
{
    if (sign * v.sign == 1)     // ha vagy pozitiv, vagy negativ mindket number
    {
        int newsign, newlength = 0;
        int maxnewlength = length > v.length ? length : v.length;
        int* newInt = new int[maxnewlength + 1];
        if (sign == -1) newsign = -1;    // negativ mindketto
        else newsign = 1;                  // pozitiv mindketto

        int i = 0, c = 0;
        while (i < length && i < v.length)
        {
            int sum = t[i] + v.t[i] + c;
            newInt[newlength] = sum % 10;
            newlength++;
            c = sum / 10;
            i++;
        }

        while (i < length)
        {
            int sum = t[i] + c;
            newInt[newlength] = sum % 10;
            newlength++;
            c = sum / 10;
            i++;
        }

        while (i < v.length)
        {
            int sum = v.t[i] + c;
            newInt[newlength] = sum % 10;
            newlength++;
            c = sum / 10;
            i++;
        }

        if (c)
        {
            newInt[newlength] = c;
            newlength++;
        }

        BigInt temp;
        temp.sign = newsign;
        temp.length = newlength;
        temp.t = new int[temp.length];
        for (int i = 0; i < newlength; i++)
            temp.t[i] = newInt[i];

        delete[] newInt;
        return temp;
    }
    else if (sign * v.sign == -1)    // ha kizarolag az egyik number negativ
    {
        if (sign == -1)
        {
            BigInt temp1(v), temp2 = *this;
            temp2.sign = 1;
            return temp1 - temp2;
        }
        else
        {
            BigInt temp1 = v, temp2(*this);
            temp1.sign = 1;
            return temp2 - temp1;
        }
    }
    else
    {
        if (sign == 0) return v;
        else return *this;
    }
}

BigInt BigInt::operator*(const BigInt& v)
{
    BigInt result;
    if (v == result || *this == result)
        return result;    // ha 0

    result.sign = sign * v.sign;
    result.length = length + v.length;
    result.t = new int[length + v.length];

    for (int i = 0; i < result.length; i++) result.t[i] = 0;

    for (int i = 0; i < length; i++)
    {
        for (int j = 0; j < v.length; j++)
        {
            result.t[i + j] += t[i] * v.t[j];
            result.t[i + j + 1] = result.t[i + j + 1] + result.t[i + j] / 10;
            result.t[i + j] %= 10;
        }
    }

    while (result.t[result.length - 1] == 0) result.length--;

    return result;
}

BigInt& BigInt::operator++()
{
    BigInt one(1);
    *this = *this + one;
    return *this;
}

BigInt BigInt::operator++(int)
{
    BigInt ret(*this), one(1);
    *this = *this + one;
    return ret;
}

BigInt& BigInt::operator--()
{
    BigInt one(1);
    *this = *this - one;
    return *this;
}

BigInt BigInt::operator--(int)
{
    BigInt ret(*this), one(1);
    *this = *this - one;
    return ret;
}

BigInt BigInt::operator/(const BigInt& v)
{
    BigInt result;
    if (v.sign == 0) throw DivisionByZero();    // 0-val osztas
    if (v.length == 1 && v.t[0] == 1)
    {
        if (v.sign == 1) return *this;          // number / 1
        else                                    // number / (-1)
        {
            result = *this;
            result.sign = -sign;
            return result;
        }
    }

    // altalanos eset:
    result.sign = sign * v.sign;
    result.t = new int[length];
    // for (int i = 0; i < length; i++) result.t[i] = 0;

    BigInt prefix, oszto(v), p10(10);     // az result signe ismert, az oszto absz. erteket taroljuk
    oszto.sign = 1;

    int i = 0, elength = 0;
    while (i < length)
    {
        int lehozva = 0;
        if (prefix.sign == 0)
        {
            delete[] prefix.t;
            prefix.sign = 1;
            prefix.t = new int[length];
            for (int i = 0; i < length; i++) prefix.t[i] = 0;
            prefix.t[0] = t[length - i - 1];
            i++;
            lehozva = 1;
        }

        while (oszto > prefix && i < length && (!lehozva || !elength))      // ha i == length, az osztas vegere ertunk
        {                                                                   // nem lep be, ha mar lehoztunk one new numberjegyet es nem az elso prefixnel vagyunk
            prefix = prefix * p10;
            prefix.t[0] = t[length - i - 1];
            i++;
            if (elength > 0) break;
        }

        BigInt szorzo(1), seged;
        while (prefix > szorzo * oszto)
        {
            seged = szorzo * oszto;
            ++szorzo;
        }
        if (prefix == szorzo * oszto) seged = szorzo * oszto;
        if (szorzo * oszto > prefix) --szorzo;
        // szorzo = ahanyszor az oszto belefer a prefixbe
        result = result * p10;
        result.t[0] = szorzo.t[0];    elength++;
        prefix = prefix - seged;
        if (i == length && oszto > prefix) break;
    }
    result.length = elength;
    if (elength == 0)
    {
        BigInt null;
        return null;
    }
    if (elength == 1 && result.t[0] == 0) result.sign = 0;
    return result;
}

BigInt& BigInt::operator+=(const BigInt& v)
{
    *this = *this + v;
    return *this;
}

BigInt& BigInt::operator-=(const BigInt& v)
{
    *this = *this - v;
    return *this;
}

BigInt& BigInt::operator*=(const BigInt& v)
{
    *this = *this * v;
    return *this;
}

BigInt& BigInt::operator/=(const BigInt& v)
{
    *this = *this / v;
    return *this;
}

ostream& BigInt::kiir(ostream& s)
{
    if (sign == -1) s << "-";
    for (int i = length - 1; i >= 0; i--)
        s << t[i];
    return s;
}

ostream& operator <<(ostream& s, BigInt& out)
{
    return out.kiir(s);
}

int main()
{
    int a[] = { 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 };
    int egesz = 2000;
    BigInt x(1, 20, a), y(egesz);

    /* Int beolvasas kezenfekvobb teszteles miatt;
    int a, b;
    cout << "a = ", cin >> a;
    cout << "b = ", cin >> b;
    cout << "x, y --> a es b numberok BigInt megfeleloi\n\n";
    BigInt x(a), y(b);
    */

    long h_egesz = LONG_MAX;
    BigInt null, masoltX(x);
    BigInt hosszu(h_egesz);

    cout << "adattagokat inicializalo konstruktor:\nx = " << x;
    cout << "\n\nimplicit atalakitas egeszrol:\ny = " << y;
    cout << "\n\nimplicit atalakitas hosszu egeszrol:\nhosszu = " << hosszu;
    cout << "\n\nmasolo konstruktor:\nmasoltX = " << masoltX;
    cout << "\n\nalapertelmezett konstruktor:\nnull = " << null << "\n\n";

    cout << "***Az elore megirt foprogram az x es y objektumokkal dolgozik.***\n\n";

    BigInt actualResult;

    actualResult = x + y;
    cout << "x + y = " << actualResult << "\n";

    actualResult = x - y;
    cout << "x - y = " << actualResult << "\n";

    actualResult = x * y;
    cout << "x * y = " << actualResult << "\n";

    try {
        actualResult = x / y;
        // cout << "\na / b = " << a / b;
        cout << "x / y = " << actualResult << "\n\n";
    }
    catch (BigInt::DivisionByZero)
    {
        cout << "\nNullaval valo osztas!\n\n";
    }

    BigInt temp = y++;
    cout << "y++ hatas: visszateritve = " << temp << "; y erteke ezutan = " << y << "\n";
    temp = ++y;
    cout << "++y hatas: visszateritve = " << temp << "; y erteke ezutan = " << y << "\n";
    temp = y--;
    cout << "y-- hatas: visszateritve = " << temp << "; y erteke ezutan = " << y << "\n";
    temp = --y;
    cout << "--y hatas: visszateritve = " << temp << "; y erteke ezutan = " << y << "\n\n";

    x = y;
    cout << "x = y kovetkezmenye: x = " << x << "; y = " << y << "\n";
    x += y;
    cout << "x += y kovetkezmenye: x = " << x << "; y = " << y << "\n";
    y -= x;
    cout << "y -= x kovetkezmenye: x = " << x << "; y = " << y << "\n";
    x *= y;
    cout << "x *= y kovetkezmenye: x = " << x << "; y = " << y << "\n";
    try {
        x /= y;
        cout << "x /= y kovetkezmenye: x = " << x << "; y = " << y << "\n\n";
    }
    catch (BigInt::DivisionByZero)
    {
        cout << "\nNullaval valo osztas!\n";
    }

    x += y + y;
    cout << "Legyen x += y + y ( ==> x == 0 )\nEkkor az y /= x muvelet eredmenye:";
    try {
        y /= x;
        // cout << "\na / b = " << a / b;
        cout << "y = " << y << "\n\n";
    }
    catch (BigInt::DivisionByZero)
    {
        cout << "\nNullaval valo osztas!\n";
    }
}
